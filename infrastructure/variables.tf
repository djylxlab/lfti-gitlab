############################################
##             INFRASTRUCTURE             ##
############################################

variable "location" {
  type        = string
  default     = "francecentral"
  description = "location of the resource group"
}

variable "RGName" {
  type        = string
  default     = "OCC_ASD_Jacques"
  description = "Name of the Resource Group"
}

variable "vnet" {
  type        = string
  default     = "Vnet_OCC_ASD_Jacques"
  description = "Name of the virtual network"
}

variable "vnetAddress" {
  type        = list(string)
  default     = ["10.0.11.0/24"]
  description = "Private ip range of the Virtual Network"
}

variable "subnet1" {
  type        = string
  default     = "AzureFirewallSubnet"
  description = "Name of the first subnet"
}

variable "subnet1Address" {
  type        = list(string)
  default     = ["10.0.11.0/26"]
  description = "Private ip range of the first subnet"
}

variable "subnet2" {
  type        = string
  default     = "Subnet2_OCC_ASD_Jacques"
  description = "Name of the second subnet"
}

variable "subnet2Address" {
  type        = list(string)
  default     = ["10.0.11.64/26"]
  description = "Private ip range of the second subnet"
}

variable "subnet3" {
  type        = string
  default     = "Subnet3_OCC_ASD_jacques"
  description = "Name of the third subnet"
}

variable "subnet3Address" {
  type        = list(string)
  default     = ["10.0.11.128/26"]
  description = "Private ip range of the third subnet"
}

variable "subnet4" {
  type        = string
  default     = "Subnet4_OCC_ASD_jacques"
  description = "Name of the fourth subnet"
}

variable "subnet4Address" {
  type        = list(string)
  default     = ["10.0.11.192/26"]
  description = "Private ip range of the fourth Subnet"
}

############################################
##                FIREWALL                ##
############################################

variable "ipFw" {
  type        = string
  default     = "lfti_ip_fw"
  description = "Name of the public ip to firewall"
}

variable "azureFirewall" {
  type        = string
  default     = "LftiFirewall"
  description = "Name of the firewall"
}

############################################
##            VIRTUALS MACHINES           ##
############################################

variable "nicMaster" {
  type        = string
  default     = "interfaceMaster"
  description = "Name of the first network interface of Virtual Machine"
}

variable "nicWorker" {
  type        = string
  default     = "interfaceWorker"
  description = "Name of the second network interface of Virtual Machine"
}


variable "vm1" {
  type        = string
  default     = "K8sMaster"
  description = "Name of the first Virtual Machine"
}

variable "vm2" {
  type        = string
  default     = "K8sWorker"
  description = "Name of the second Virtual Machine"
}

variable "username" {
  type        = string
  default     = "jacques"
  description = "Name of User"
}

############################################
##                DATABASE                ##
############################################

variable "MySql" {
  type        = string
  default     = "msqlfornextcloudjack"
  description = "Name of the MySql server"
}

variable "DbName" {
  type        = string
  default     = "nextcloud"
  description = "Name of the database"
}

variable "loginmsql" {
  type        = string
  default     = "nextcloud"
  description = "Name of the postgresql's user"
}

variable "passwdmsql" {
  type      = string
  sensitive = true
}
