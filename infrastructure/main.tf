####################################################
##                 RESOURCE GROUP                 ##
####################################################

resource "azurerm_resource_group" "rg" {
  location = var.location
  name     = var.RGName
}

####################################################
##                   INFRASTRUCTURE               ##
####################################################

resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = var.vnetAddress
}

resource "azurerm_subnet" "subnet1" {
  name                 = var.subnet1
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.subnet1Address
}

resource "azurerm_subnet" "subnet2" {
  name                 = var.subnet2
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.subnet2Address
}

resource "azurerm_subnet" "subnet3" {
  name                 = var.subnet3
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.subnet3Address
}

resource "azurerm_subnet" "subnet4" {
  name                 = var.subnet4
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.subnet4Address
  service_endpoints    = ["Microsoft.Storage"]
  delegation {
    name = "fs"
    service_delegation {
      name = "Microsoft.DBforMySQL/flexibleServers"
      actions = [
        "Microsoft.Network/virtualNetworks/subnets/join/action",
      ]
    }
  }
}

####################################################
##                    FIREWALL                    ##
####################################################

resource "azurerm_public_ip" "ipFw" {
  name                = var.ipFw
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_firewall" "azureFirewall" {
  name                = var.azureFirewall
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  sku_name            = "AZFW_VNet"
  sku_tier            = "Standard"

  ip_configuration {
    name                 = "configurationFw"
    subnet_id            = azurerm_subnet.subnet1.id
    public_ip_address_id = azurerm_public_ip.ipFw.id
  }
}

resource "azurerm_firewall_nat_rule_collection" "dnat1" {
  name                = "natRuleSsh1"
  resource_group_name = azurerm_resource_group.rg.name
  azure_firewall_name = azurerm_firewall.azureFirewall.name
  priority            = 100
  action              = "Dnat"

  rule {
    name = "sshVmMaster"

    source_addresses = [
      "*",
    ]

    destination_ports = [
      "22",
    ]

    destination_addresses = [
      azurerm_public_ip.ipFw.ip_address
    ]

    translated_port = 22

    translated_address = azurerm_linux_virtual_machine.masterk8s.private_ip_address

    protocols = [
      "TCP",
    ]
  }
}

resource "azurerm_firewall_nat_rule_collection" "dnat2" {
  name                = "natRuleSsh2"
  resource_group_name = azurerm_resource_group.rg.name
  azure_firewall_name = azurerm_firewall.azureFirewall.name
  priority            = 110
  action              = "Dnat"

  rule {
    name = "sshVmWorker"

    source_addresses = [
      "*",
    ]

    destination_ports = [
      "2222",
    ]

    destination_addresses = [
      azurerm_public_ip.ipFw.ip_address
    ]

    translated_port = 22

    translated_address = azurerm_linux_virtual_machine.workerk8s.private_ip_address

    protocols = [
      "TCP",
    ]
  }
}

resource "azurerm_firewall_nat_rule_collection" "dnat3" {
  name                = "natRuleHttp"
  resource_group_name = azurerm_resource_group.rg.name
  azure_firewall_name = azurerm_firewall.azureFirewall.name
  priority            = 120
  action              = "Dnat"

  rule {
    name = "HttpRedirection"

    source_addresses = [
      "*",
    ]

    destination_ports = [
      "80",
    ]

    destination_addresses = [
      azurerm_public_ip.ipFw.ip_address
    ]

    translated_port = 31882

    translated_address = azurerm_linux_virtual_machine.workerk8s.private_ip_address

    protocols = [
      "TCP",
    ]
  }
}

####################################################
##                VIRTUALS MACHINES               ##
####################################################

resource "azurerm_network_interface" "nicMaster" {
  name                = var.nicMaster
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = azurerm_subnet.subnet3.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "masterk8s" {
  name                = var.vm1
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  size                = "Standard_B2s"
  admin_username      = var.username

  network_interface_ids = [
    azurerm_network_interface.nicMaster.id
  ]

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "tidalmediainc"
    offer     = "docker-compose-rocky-9"
    sku       = "docker-compose-rocky-9"
    version   = "1.0.0"
  }

  plan {
    publisher = "tidalmediainc"
    product   = "docker-compose-rocky-9"
    name      = "docker-compose-rocky-9"
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
}

resource "azurerm_network_interface" "nicWorker" {
  name                = var.nicWorker
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "ipconfig2"
    subnet_id                     = azurerm_subnet.subnet3.id
    private_ip_address_allocation = "Dynamic"
  }
  depends_on = [azurerm_network_interface.nicMaster]
}


resource "azurerm_linux_virtual_machine" "workerk8s" {
  name                = var.vm2
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  size                = "Standard_B2s"
  admin_username      = var.username

  network_interface_ids = [
    azurerm_network_interface.nicWorker.id
  ]

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "tidalmediainc"
    offer     = "docker-compose-rocky-9"
    sku       = "docker-compose-rocky-9"
    version   = "1.0.0"
  }

  plan {
    publisher = "tidalmediainc"
    product   = "docker-compose-rocky-9"
    name      = "docker-compose-rocky-9"
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
  depends_on = [azurerm_linux_virtual_machine.masterk8s]
}

####################################################
##                     MYSQL                      ##
####################################################

resource "azurerm_private_dns_zone" "dnsName" {
  name                = "lfti.mysql.database.azure.com"
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_private_dns_zone_virtual_network_link" "linkDns" {
  name                  = "lftiDjylxlink.com"
  private_dns_zone_name = azurerm_private_dns_zone.dnsName.name
  virtual_network_id    = azurerm_virtual_network.vnet.id
  resource_group_name   = azurerm_resource_group.rg.name
  depends_on            = [azurerm_subnet.subnet4]
}

resource "azurerm_mysql_flexible_server" "MySql" {
  name                   = var.MySql
  resource_group_name    = azurerm_resource_group.rg.name
  location               = var.location
  version                = "8.0.21"
  delegated_subnet_id    = azurerm_subnet.subnet4.id
  private_dns_zone_id    = azurerm_private_dns_zone.dnsName.id
  administrator_login    = var.loginmsql
  administrator_password = var.passwdmsql
  sku_name               = "GP_Standard_D2ds_v4"
  depends_on             = [azurerm_private_dns_zone_virtual_network_link.linkDns]
}

resource "azurerm_mysql_flexible_database" "DbMysql" {
  name                = var.DbName
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_mysql_flexible_server.MySql.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}
