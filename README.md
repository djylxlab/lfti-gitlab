# Welcome to my LFTI repository

This repository is my final project during my Adminsitrator System Devops training. \
This is a Secure Cluster Kubernetes deployment on Microsoft Azure.

At the time i'm writting this file, i didn't finish the project, but i will continue it for completing the missing parts and i will rectify the "README".

## Project Objective

The aim is the a Nextcloud application deployment on a Kubernetes Cluster. \
This my first use to Kubernetes so i didn't used the Azure PAAS Solution and i create a bare-metal cluster to get to grips with the tools.

Nextcloud is an application for collaborative document sharing and management.

My repository contains many folder :

### Infrastructure

In this folder, you will find all the necessary for deploying the infrastructure with **Terraform** build automation. \
For running it : 
 - `terraform init`
 - `terraform apply -var-file=secrets.tfvars`

This script deploy:

 - 1 **resource group named** `OCC_ASD_Jacques`
 - 1 **Virtual Newtork** named `Vnet_OCC_ASD_Jacques` with a private IP `10.0.11.0/24`
 - 4 **Subnets**. `10.0.11.0/26`, `10.0.11.64/26`, `10.0.11.128/26` and `10.0.11.192/26`
 - 1 **Firewall** with 2 **dnat** rules for **ssh** protocol and 1 **dnat** rule for **http**
 - 2 **Linux Virtual Machines** (**Rocky Linux**)
 - 1 **DNS zone**
 - 1 **Private Link**
 - 1 **MySQL Flexible server** with a **database**

### Provisionning
